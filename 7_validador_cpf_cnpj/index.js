function myMask(objeto, mascara) {
    obj = objeto
    masc = mascara
    setTimeout("myMaskEx()", 1)
}
function myMaskEx() {
    obj.value = masc(obj.value)
}

function formatarCnpj(cnpj) {
    return cnpj.replace(/\D/g, "")
        .substring(0, 14)
        .replace(/^(\d{2})(\d)/, "$1.$2")
        .replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3")
        .replace(/\.(\d{3})(\d)/, ".$1/$2")
        .replace(/(\d{4})(\d)/, "$1-$2")
}

function formatarCpf(cpf) {
    return cpf.replace(/\D/g, "")
        .substring(0, 11)
        .replace(/(\d{3})(\d)/, "$1.$2")
        .replace(/(\d{3})(\d)/, "$1.$2")
        .replace(/(\d{3})(\d{1,2})$/, "$1-$2")
}

function clearNum(num) {
    return num.replace(/\D/g, "")
}

function validarCnpj() {
    cnpj = document.getElementById('input-cnpj').value
    cnpj = clearNum(cnpj)
}

function uniq(arr) {
    var seen = {};
    return arr.filter(function (item) {
        return seen.hasOwnProperty(item) ? false : (seen[item] = true);
    });
}

function validarCnpj() {
    var cnpj = document.getElementById('input-cnpj').value
    if (isCnpj(cnpj)) {
        document.getElementById('output').innerHTML = "Cnpj válido!!"
    } else {
        document.getElementById('output').innerHTML = "Cnpj inválido!!"
    }
}

function isCnpj(cnpj) {
    cnpj = clearNum(cnpj)
    if (cnpj.length !== 14) return false
    // Se todos numeros forem iguals, retorna falso
    if (uniq(Array.from(cnpj)).length == 1) return false

    const checkDigit13 = [2, 3, 4, 5, 6, 7, 8, 9, 2, 3, 4, 5]
    const checkDigit14 = [2, 3, 4, 5, 6, 7, 8, 9, 2, 3, 4, 5, 6]

    var acc = []
    var partialCnpj = cnpj.substring(0, 12)

    acc = Array.from(partialCnpj).reverse().map((num, index) => {
        return num * checkDigit13[index]
    })

    var sumAcc = acc.reduce((ac, value) => {
        return ac + value
    }, 0)

    var accPlus = []
    var partial14Cnpj = cnpj.substring(0, 13)

    accPlus = Array.from(partial14Cnpj).reverse().map((num, index) => {
        return num * checkDigit14[index]
    })

    var sumAccPlus = accPlus.reduce((ac, value) => {
        return ac + value
    }, 0)

    var digitoEncontrado = ((sumAcc * 10) % 11) % 10
    var digitoEncontradoPlus = ((sumAccPlus * 10) % 11) % 10

    if (digitoEncontrado == cnpj.substring(12, 13) && digitoEncontradoPlus == cnpj.substring(13, 14)) return true
    return false
}

function validarCpf() {
    var cpf = document.getElementById('input-cpf').value
    if (isCpf(cpf)) {
        document.getElementById('output').innerHTML = "CPF válido!!"
    } else {
        document.getElementById('output').innerHTML = "CPF inválido!!"
    }
}

function isCpf(cpf) {
    cpf = clearNum(cpf)
    if (cpf.length !== 11) return false
    // Se todos numeros forem iguals, retorna falso
    if (uniq(Array.from(cpf)).length == 1) return false

    const checkDigit10 = [10, 9, 8, 7, 6, 5, 4, 3, 2]
    const checkDigit11 = [11, 10, 9, 8, 7, 6, 5, 4, 3, 2]

    var acc = []
    var partialCpf = cpf.substring(0, 9)

    acc = Array.from(partialCpf).map((num, index) => {
        return num * checkDigit10[index]
    })

    var sumAcc = acc.reduce((ac, value) => {
        return ac + value
    }, 0)

    var accPlus = []
    var partial10Cpf = cpf.substring(0, 10)

    accPlus = Array.from(partial10Cpf).map((num, index) => {
        return num * checkDigit11[index]
    })

    var sumAccPlus = accPlus.reduce((ac, value) => {
        return ac + value
    }, 0)

    var digitoEncontrado = ((sumAcc * 10) % 11) % 10
    var digitoEncontradoPlus = ((sumAccPlus * 10) % 11) % 10

    if (digitoEncontrado == cpf.substring(9, 10) && digitoEncontradoPlus == cpf.substring(10, 11)) return true
    return false
}

function inputCpf() {
    document.getElementById("label-cpf").removeAttribute('hidden')
    document.getElementById("label-cnpj").setAttribute('hidden', true)
}

function inputCnpj() {
    document.getElementById("label-cnpj").removeAttribute('hidden')
    document.getElementById("label-cpf").setAttribute('hidden', true)
}
