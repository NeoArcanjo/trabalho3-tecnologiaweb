const converter = (text) => {
    var aux = text
        .replace(/I(?=(X|V))/g, "-1")
        .replace(/I/g, "+1")
        .replace(/V/g, "+5")
        .replace(/X(?=(L|C))/g, "-10")
        .replace(/X/g, "+10")
        .replace(/L/g, "+50")
        .replace(/C/g, "+100")
    return eval(aux)
}

document.getElementById("input").addEventListener('keyup', function (e) {
    // if (e.charCode === 13) e.preventDefault()
    // var newText = (e.charCode === 0 ) ? '' : e.key
    var text = document.getElementById("input").value.toUpperCase()
    document.getElementById("output").value = converter(text)
})