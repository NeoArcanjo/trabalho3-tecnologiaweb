document.getElementById("input").addEventListener('keyup', function (e) {
    // caracteres, palavras, espaços, linhas, vogais e números.
    // Deve fazer a contagem ao alterar o texto ou digitar algo
    var text = document.getElementById("input").value
    document.getElementById("output").value = higienize(text)
})

function higienize(text){
    console.log(text)
    var output = text
        .replace(/[aáàãâ]/g, "a")
        .replace(/[AÁÀÃÂ]/g, "A")
        .replace(/[eéê]/g, "e")
        .replace(/[EÉÊ]/g, "E")
        .replace(/[iíî]/g, "i")
        .replace(/[IÍÎ]/g, "I")
        .replace(/[oóôõ]/g, "o")
        .replace(/[OÓÔÕ]/g, "O")
        .replace(/[uúûü]/g, "u")
        .replace(/[UÚÛÜ]/g, "U")

    return output
}