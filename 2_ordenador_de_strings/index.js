function sortAndShow(textArray) {
    var output = textArray.filter((word) => {
        return (word != "" && word != " ")
    }).sort().join("\n")
    document.getElementById('output').innerHTML = output
}

function orderByWords(text) {
    var textArray = text.replace(/\W+/, ' ').split(" ")
    sortAndShow(textArray)
}

function orderByLines(text) {
    var textArray = text.split('\n')
    sortAndShow(textArray)
}

var orderByPalavras = document.getElementById("orderby-palavras")
var orderByLinhas = document.getElementById("orderby-linhas")
var orderControl = "palavras"

orderByLinhas.addEventListener('change', function (e) {
    orderControl = e.srcElement.value
    var reloadText = document.getElementById("input").value
    orderByLines(reloadText)
})

orderByPalavras.addEventListener('change', function (e) {
    orderControl = e.srcElement.value
    var reloadText = document.getElementById("input").value
    orderByWords(reloadText)
})

var text = "";

document.getElementById("input").addEventListener('keyup', function (e) {
    text = document.getElementById("input").value
    if (orderControl === "palavras") {
        orderByWords(text)
    } else {
        orderByLines(text)
    }
})
