document.getElementById("input").addEventListener('keyup', function (e) {
    // caracteres, palavras, espaços, linhas, vogais e números.
    // Deve fazer a contagem ao alterar o texto ou digitar algo
    var text = document.getElementById("input").value
    refreshCount(text)
})

function refreshCount(text) {
    console.log(text)
    var output = text
        .length + ' caracter(es) \n'
    output += (text
        .match(/\d/g) || [])
        .length + ' numero(s) \n'
    output += (text
        .match(/\s/g) || [])
        .length + ' espaço(s) em branco\n'
    output += (text
        .match(/[AaÁáÀàÃãÂâEeÉéÊêIiÍíÎîOoÓóÔôÕõUuÚúÛûÜü]/g) || [])
        .length + ' vogal(is) \n'
    output += text
        .replace(/[!"#$%&'()*+,\-./:;<=>?@[\]^_`{|}~]+/, " ")
        .replace(/\s+/, " ")
        .split(/[\r\n\s]/)
        .filter(value => { return value != "" })
        .length + ' palavra(s) \n'
    var test = text
    console.log(test)












    document.getElementById('output').innerHTML = output
}