function clearMemory() {
    var display = document.getElementById('display')
    display.innerText = '0'
}

function setOperation(operation) {
    var display = document.getElementById('display')
    var memory = document.getElementById('memory-content')

    var values = (display.innerText).split(/[^\d\.]/)
    values = values.filter((term) => {  return term.match(/\d+/) })
    if (values.length > 1) {
        let historical = document.createElement('span')
        historical.innerHTML = display.innerHTML
        display.innerHTML = eval(display.innerHTML).toFixed(3)
        display.innerHTML = (Math.fround(display.innerHTML) == display.innerHTML) ? Math.fround(display.innerHTML) : display.innerHTML
        historical.innerHTML += "=" + display.innerHTML + '</br>'
        memory.appendChild(historical)
    }

    if (operation !== '=') {
        display.innerHTML = display.innerHTML.replace(/\D$/, '')
        display.innerHTML += operation
    }
}

function addDigit(n) {
    var display = document.getElementById('display')
    var values = (display.innerText).split(/[^\d\.]/)
    values = values.filter((term) => { return term.match(/\d+/) })
    if (n === '.' && values[values.length - 1].includes(".")) {
        return
    }

    const clearDisplay = display.innerHTML === '0'
    display.innerHTML = clearDisplay ? '' : display.innerHTML
    display.innerHTML += n
}

var content = `<div class="calculator">
                    <div class="display" id="display"></div>
                    <button value="AC" onClick="clearMemory()" class="triple">AC</button>
                    <button value="/" onClick="setOperation('/')" class="operation">/</button>
                    <button value="7" onClick="addDigit(7)">7</button>
                    <button value="8" onClick="addDigit(8)">8</button>
                    <button value="9" onClick="addDigit(9)">9</button>
                    <button value="*" onClick="setOperation('*')" class="operation">x</button>
                    <button value="4" onClick="addDigit(4)">4</button>
                    <button value="5" onClick="addDigit(5)">5</button>
                    <button value="6" onClick="addDigit(6)">6</button>
                    <button value="-" onClick="setOperation('-')" class="operation">-</button>
                    <button value="1" onClick="addDigit(1)">1</button>
                    <button value="2" onClick="addDigit(2)">2</button>
                    <button value="3" onClick="addDigit(3)">3</button>
                    <button value="+" onClick="setOperation('+')" class="operation">+</button>
                    <button value="0" onClick="addDigit(0)" class="double">0</button>
                    <button value="." onClick="addDigit('.')">.</button>
                    <button value="=" onClick="setOperation('=')" class="operation">=</button>
                </div>`

var root = document.getElementById('app')
root.innerHTML = content

