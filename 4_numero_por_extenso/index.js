var unidade = ["zero","um","dois","tres","quatro","cinco","seis","sete","oito","nove"];
var unidadesDez = ["dez","onze","doze","treze","quatorze","quinze","dezesseis","dezessete","dezoito","dezenove"];
var dezena = ["", "","vinte","trinta","quarenta","cinquenta","sessenta","setenta","oitenta","noventa"];
var centena = ["","cento","duzentos","trezentos","quatrocentos","quinhentos","seicentos","oitocentos","novecentos"];
var n1;
var n2;

function reset()
{
    var resetButton = document.getElementById(input);
    if(resetButton){
        resetButton.value= "";
   }
}

function traduzir(){
    n1 = document.getElementById("input").value;

    if(n1.length < 2)
    {
        n2 = unidade[n1];
    }
    else if(n1.length == 2)
    {
        var firstNum = n1.slice(0,1) ;
        var secondNum = n1.slice(1,2) ;
        if(firstNum != 1)
        {
            if(secondNum != 0){
                n2 = dezena[firstNum] + " e " + unidade[secondNum];
            }
            else{
                n2 = dezena[firstNum];
            }
        }
        else
        {
            n2 = unidadesDez[secondNum];
        }
        
        
    }
    else if(n1.length == 3)
    {
        var firstNum = n1.slice(0,1) ;
        var secondNum = n1.slice(1,2) ;
        var thirdNum = n1.slice(2,3);

        if(firstNum == 1 && secondNum == 0 && thirdNum == 0)
        {
            n2 = "cem";
        }
        else
        {
            if(secondNum != 0)
            {
                if(secondNum == 1)
                {
                    n2 = centena[firstNum] + " e " + unidadesDez[thirdNum]; 
                }
                if(thirdNum != 0 && secondNum !=1)
                {
                    n2 = centena[firstNum] + " e " + dezena[secondNum] + " e " + unidade[thirdNum];
                }
                else if(thirdNum == 0 && secondNum != 1)
                {
                    n2 = centena[firstNum] + " e " + dezena[secondNum];
                }
            }
            else
            {
                n2 = centena[firstNum] + " e " + unidade[thirdNum];
            }
        }

    }
    else if(n1.length == 4)
    {
        var firstNum = n1.slice(0,1);
        var secondNum = n1.slice(1,2);
        var thirdNum = n1.slice(2,3);
        var fourthNum = n1.slice(3,4);

        if(fourthNum == 0 )
        {
            if(firstNum != 1)
            {
                if(secondNum != 0)
                {
                    if(thirdNum == 1){
                        n2 = unidade[firstNum] + " mil " + centena[secondNum] + " e " + unidadesDez[0]
                    }
                    else
                    {
                        n2 = unidade[firstNum] + " mil " + centena[secondNum] + " e " + dezena[thirdNum]
                    }
                }
                else
                {
                    if(thirdNum == 1){
                        n2 = unidade[firstNum] + " mil " + " e " + unidadesDez[0]
                    }
                    else
                    {
                        n2 = unidade[firstNum] + " mil " + " e " + dezena[thirdNum]
                    }
                }
            }
            else
            {
                if(secondNum != 0)
                {
                    if(thirdNum == 1){
                        n2 =  " mil " + centena[secondNum] + " e " + unidadesDez[0]
                    }
                    else
                    {
                        n2 =  " mil " + centena[secondNum] + " e " + dezena[thirdNum]
                    }
                }
                else
                {
                    if(thirdNum == 1){
                        n2 =  " mil " + " e " + unidadesDez[0]
                    }
                    else
                    {
                        n2 =  " mil " + " e " + dezena[thirdNum]
                    }
                }
            }
            if(thirdNum == 0)
            {
                if(firstNum != 1){
                    if(secondNum != 1){
                        n2 = unidade[firstNum] + " mil e " + centena[secondNum];
                    }
                    else{
                        n2 = unidade[firstNum] + " mil e cem";
                    }
                }
                else{
                    
                    if(secondNum != 1){
                        n2 = "mil e " + centena[secondNum];
                    }
                    else{
                        n2 = "mil e cem";
                    }
                }

                if(secondNum == 0)
                {
                    if(firstNum == 1)
                    {
                        n2 = "mil";
                    }
                    else{
                        n2 = unidade[firstNum] + " mil";
                    }
                }
            }
        }
        else
        {
            if(firstNum != 1)
            {
                if(secondNum != 0)
                {
                    if(thirdNum == 1){
                        n2 = unidade[firstNum] + " mil " + centena[secondNum] + " e " + unidadesDez[fourthNum]; 
                    }
                    else
                    {
                        n2 = unidade[firstNum] + " mil " + centena[secondNum] + " e " + dezena[thirdNum] + " e " + unidade[fourthNum];
                    }
                }
                else
                {
                    if(thirdNum == 1){
                        n2 = unidade[firstNum] + " mil " + " e " + unidadesDez[fourthNum]
                    }
                    else
                    {
                        n2 = unidade[firstNum] + " mil " + " e " + dezena[thirdNum] + " e " + unidade[fourthNum];
                    }
                }
            }
            else
            {
                if(secondNum != 0)
                {
                    if(thirdNum == 1){
                        n2 =  " mil " + centena[secondNum] + " e " + unidadesDez[fourthNum]
                    }
                    else
                    {
                        n2 =  " mil " + centena[secondNum] + " e " + dezena[thirdNum] + " e " + unidade[fourthNum];
                    }
                }
                else
                {
                    if(thirdNum == 1){
                        n2 =  " mil " + " e " + unidadesDez[fourthNum];
                    }
                    else
                    {
                        n2 =  " mil " + " e " + dezena[thirdNum] + " e " + unidade[fourthNum];
                    }
                }
            }
            if(thirdNum == 0)
            {
                if(firstNum != 1){
                    if(secondNum != 1){
                        n2 = unidade[firstNum] + " mil e " + centena[secondNum] + " e " + unidade[fourthNum];;
                    }
                    else{
                        n2 = unidade[firstNum] + " mil e cem";
                    }
                }
                else{
                    
                    if(secondNum != 1){
                        n2 = "mil e " + centena[secondNum] + " e " + unidade[fourthNum];;
                    }
                    else{
                        n2 = "mil e cem";
                    }
                }

                if(secondNum == 0)
                {
                    if(firstNum == 1)
                    {
                        n2 = "mil";
                    }
                    else{
                        n2 = unidade[firstNum] + " mil" + " e " + unidade[fourthNum];;
                    }
                }
            }
        }
        
    }

    mostrarTexto();
    reset();
    }

 function mostrarTexto()
 {
    document.getElementById('output').innerHTML = n2;
 }
 