# Trabalho 3 #

## Requisitos ##

1. Deve conter uma página index.html que será a página inicial do site.
   A página principal deve conter links para as ferramentas. &#x2713;

2. Os Sites devem possuir configuração de CSS e os códigos de Java Script em arquivos externos. 
   Cada Página deve possuir seu próprio arquivo de Java Script. &#x2713;

## Ferramentas ##

1. Calculadora Básica
    Criar uma Calculadora com as 4 operações básicas (adição, subtração, multiplicação e divisão)
    Deve possuir uma listagem dos históricos que foram realizados &#x2713;

2. Colocar em Ordem Alfabética
    Criar uma Ferramenta para ordem alfabeticamente as palavras de um texto informado. Deve ter uma opção para
    ordenar por linhas ou por palavras.
    Deve possuir uma área de entrada e outra para mostrar o texto ordenado &#x2713;

3. Inverter Texto
    Criar uma Ferramenta para inverter um texto.
    Deve possuir uma área de entrada e outra para mostrar o texto invertido. &#x2713;

4. Número por Extenso
    Criar uma Ferramenta para escrever números monetário por extenso.
    Deve digitar um número converter para um texto por extenso

5. Remover Acentos
    Criar uma Ferramenta para remover acentos de um Texto.
    Deve possuir uma área de entrada e outra para mostrar o texto sem acentos.

6. Contador de Texto
    Criar uma Ferramenta para contar informações sobre o texto informado. Deve contar a quantidade de
    caracteres, palavras, espaços, linhas, vogais e números.
    Deve fazer a contagem ao alterar o texto ou digitar algo

7. Validador de Documento
    Criar uma Ferramenta para validar CPF ou CNPJ.
    Deve possuir uma área de entrada e uma opção do tipo do documento. &#x2713;

8. Converter Números Romanos em Decimais
    Criar uma ferramenta para converter um número romano para decimal, e desconverter de um decimal para
    número romano.
    Deve possuir uma opção para indicar qual o tipo da conversão.
    Deve ter uma limitação dos números até 100.
